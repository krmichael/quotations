import React from "react";
import App from "next/app";
import { ThemeProvider } from "styled-components";
import Helmet from "react-helmet";

import isServer from "../utils/isServer";

import GlobalStyle from "../styles/global";
import ThemeContext from "../theme/context";
import * as themes from "../theme";

export default class MyApp extends App {
  state = {
    theme: themes.dark,
  };

  componentDidMount() {
    !isServer &&
      this.setState({
        theme:
          JSON.parse(localStorage.getItem("@quotation_theme")) || themes.dark,
      });
  }

  toggleTheme = () => {
    this.setState(
      (state) => ({
        theme: state.theme === themes.dark ? themes.light : themes.dark,
      }),
      () => {
        !isServer &&
          localStorage.setItem(
            "@quotation_theme",
            JSON.stringify(this.state.theme)
          );
      }
    );
  };

  render() {
    const { Component, pageProps } = this.props;

    return (
      <ThemeContext.Provider value={this.state}>
        <ThemeContext.Consumer>
          {(theme) => (
            <ThemeProvider theme={theme.theme}>
              <GlobalStyle />
              <Helmet
                htmlAttributes={{ lang: "pt-BR" }}
                title="Cotação Dólar | cotações.app"
                meta={[
                  {
                    name: "viewport",
                    content: "width=device-width, initial-scale=1",
                  },
                  {
                    property: "og:title",
                    content: "Cotação Dólar",
                  },
                ]}
              />
              <Component toggleTheme={this.toggleTheme} {...pageProps} />
            </ThemeProvider>
          )}
        </ThemeContext.Consumer>
      </ThemeContext.Provider>
    );
  }
}
