import React, { useContext } from "react";
import router from "next/router";

import styled from "styled-components";
import { WiDirectionLeft } from "react-icons/wi";

import ThemeContext from "../../theme/context";

function PageNotFound() {
  const { theme } = useContext(ThemeContext);

  return (
    <Container>
      <Header>
        <BackButton
          color={theme.foreground}
          size={40}
          onClick={() => router.push("/")}
          title="Voltar pra home!"
        />
      </Header>

      <Page>
        <Title>404 - Page Not Found</Title>
      </Page>
    </Container>
  );
}

export const Container = styled.div`
  height: 100%;
`;

export const Header = styled.div`
  padding: 15px;
`;

export const BackButton = styled(WiDirectionLeft)`
  cursor: pointer;
`;

export const Page = styled.div`
  height: calc(100% - 65px);
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Title = styled.p`
  color: ${(props) => props.theme.foreground};
`;

export default PageNotFound;
