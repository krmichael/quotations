import { useRouter } from "next/router";
import dynamic from "next/dynamic";

import jsonActive from "../data/data.json";
import isServer from "../utils/isServer";
import api from "../services/api";

import currencyFormat from "../utils/currencyFormat";

import Header from "../components/Header";
import Content from "../components/Content";
import Loading from "../components/Loading";

const Card = dynamic(() => import("../components/Card"), {
  loading: () => <Loading>Loading card...</Loading>,
  ssr: false,
});

function Coin({ error, filtered, names, slug, toggleTheme }) {
  const router = useRouter();

  if ((!isServer && error) || (!isServer && !filtered.length))
    router.replace("/404", "/page-not-found");

  const content = jsonActive[slug];

  return (
    <>
      <Header
        data={names}
        slug={slug}
        breadcrumb={true}
        toggleTheme={toggleTheme}
      />

      {filtered?.map((coin) => (
        <Card
          key={coin.code + coin.codein}
          title={`${coin.name} hoje`}
          cash={currencyFormat({ value: coin.high })}
          codeActive={`${coin.code}-${coin.codein}`.toLowerCase()}
        />
      ))}

      <Content content={content} />
    </>
  );
}

export async function getServerSideProps({ params }) {
  try {
    const { data } = await api.get(`/json/all`);

    let coins = [];

    for (let coin in data) {
      coins.push(data[coin]);
    }

    const names = coins?.map((item) => item.name);

    const filtered = coins.filter(
      (coin) =>
        coin.name.toLowerCase().replace(/\s/g, "-") === params.quotationPage
    );

    return {
      props: { filtered, names, slug: params.quotationPage },
    };
  } catch (error) {
    return {
      props: { error: true },
    };
  }
}

export default Coin;
