import Document, { Head, Main, NextScript } from "next/document";

import Helmet from "react-helmet";
import { ServerStyleSheet } from "styled-components";

export default class Doc extends Document {
  static async getInitialProps(ctx) {
    const sheet = new ServerStyleSheet();
    const originalRenderPage = ctx.renderPage;

    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: (App) => (props) =>
            sheet.collectStyles(<App {...props} />),
        });

      const initialProps = await Document.getInitialProps(ctx);
      return {
        ...initialProps,
        styles: (
          <>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        ),
        helmet: Helmet.renderStatic(),
      };
    } finally {
      sheet.seal();
    }
  }

  //Deve renderizar no html
  get helmetHtmlAttrComponents() {
    return this.props.helmet.htmlAttributes.toComponent();
  }

  //Deve renderizar no body
  get helmetBodyAttrComponents() {
    return this.props.helmet.bodyAttributes.toComponent();
  }

  //Deve renderizar no head
  get helmetHeadComponents() {
    return Object.keys(this.props.helmet)
      .filter((elem) => elem !== "htmlAttributes" && elem !== "bodyAttributes")
      .map((elem) => this.props.helmet[elem].toComponent());
  }

  render() {
    return (
      <html {...this.helmetHtmlAttrComponents}>
        <Head>{this.helmetHeadComponents}</Head>

        <body {...this.helmetBodyAttrComponents}>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
