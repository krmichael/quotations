import Layout from "../../components/Layout";
import Analyze from "../../components/Analyze";

export default function Testing() {
  return (
    <Layout>
      <Analyze autosize symbol={"NASDAQ:TSLA"} />
    </Layout>
  );
}
