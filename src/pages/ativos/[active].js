import React, { useEffect } from "react";
import { useRouter } from "next/router";
import dynamic from "next/dynamic";
import Helmet from "react-helmet";

import isServer from "../../utils/isServer";

import api from "../../services/api";
import currencyFormat from "../../utils/currencyFormat";
import scrollToTop from "../../utils/scrollToTop";
import dataActive from "../../data/data.json";

import Header from "../../components/Header";
import Sidebar from "../../components/Sidebar";
import TickerTape from "../../components/TickerTape";
import Loading from "../../components/Loading";
import Content from "../../components/Content";

const Card = dynamic(() => import("../../components/Card"), {
  loading: () => <Loading>Loading card...</Loading>,
  ssr: false,
});

function QuotationActive({ error, item }) {
  const router = useRouter();

  if (!isServer && error) router.replace("/ativos");

  const contentActive = dataActive[item.name.replace(/\s/g, "-").toLowerCase()];

  useEffect(() => {
    scrollToTop();
  }, []);

  return (
    <>
      <Helmet
        title={`Cotação ${item.name} | cotações.app`}
        meta={[
          {
            property: "og:title",
            content: `Cotação ${item.name}`,
          },
        ]}
      />

      <Header />

      <Sidebar />

      <div
        style={{
          height: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Card
          key={item.code + item.codein}
          title={`${item.name} Agora`}
          cash={currencyFormat({ value: item.bid })}
          codeActive={`${item.code}-${item.codein}`.toLowerCase()}
        />
      </div>

      <div style={{ marginBottom: 50 }}>
        <Content content={contentActive} />
      </div>

      <TickerTape largeChartUrl="http://localhost:3000/redirect" />
    </>
  );
}

export async function getServerSideProps({ query }) {
  const regexpCode = /\D{3,}-\D{3,}/.test(query?.symbol);

  if (!query?.symbol || !regexpCode) {
    return {
      props: { error: true },
    };
  }

  try {
    const symbol = query.symbol.toUpperCase();

    const { data } = await api.get(`/json/all/${symbol}`);

    let item = {};

    for (let coin in data) {
      item = {
        code: data[coin].code,
        codein: data[coin].codein,
        name: data[coin].name,
        bid: data[coin].bid,
      };
    }

    return {
      props: { error: false, item },
    };
  } catch (error) {
    return {
      props: { error: true },
    };
  }
}

export default QuotationActive;
