import React, { useEffect } from "react";
import { useRouter } from "next/router";
import Helmet from "react-helmet";
import Img from "react-image";

import { MdArrowForward as ArrowIcon } from "react-icons/md";

import scrollToTop from "../../utils/scrollToTop";
import api from "../../services/api";
import Header from "../../components/Header";
import Sidebar from "../../components/Sidebar";
import TickerTape from "../../components/TickerTape";

function Quotation({ data }) {
  const router = useRouter();

  let coins = [];

  for (let coin in data) {
    coins.push(data[coin]);
  }

  useEffect(() => {
    scrollToTop();
  }, []);

  return (
    <>
      <Helmet
        title={`Principais ativos | cotações.app`}
        meta={[
          {
            property: "og:title",
            content: "Lista dos principais ativos para cotação",
          },
        ]}
      />

      <Header />

      <Sidebar />

      <div
        style={{
          display: "flex",
          flexWrap: "wrap",
          paddingTop: 70,
        }}
      >
        {coins?.map((item, index) => (
          <div
            className="card"
            key={item.code + index}
            style={{
              width: 300,
              border: "1px solid #ccc",
              borderRadius: 5,
              margin: 10,
              cursor: "pointer",
            }}
          >
            <div
              className="card-header"
              style={{
                display: "flex",
                justifyContent: "space-between",
                margin: "auto 10px",
                alignItems: "center",
                paddingTop: 10,
                paddingBottom: 10,
              }}
            >
              <h3>{item.name}</h3>

              <Img
                style={{ width: 34, height: 34, borderRadius: 34 }}
                src={[
                  `/images/${item.code.toLowerCase()}.svg`,
                  "/images/none.svg",
                ]}
                alt={`${item.name} logo`}
              />
            </div>
            <div
              className="card-footer"
              style={{
                display: "flex",
                justifyContent: "flex-end",
                margin: 10,
              }}
              onClick={() =>
                router.push(
                  `/ativos/[active]`,
                  `/ativos/cotacao-${item.name.replace(/\s/g, "-")}?symbol=${
                    item.code
                  }-${item.codein}`.toLowerCase()
                )
              }
            >
              <ArrowIcon color="#3fa69a" size={24} />
            </div>
          </div>
        ))}
      </div>

      <TickerTape largeChartUrl="http://localhost:3000/redirect" />
    </>
  );
}

export async function getServerSideProps() {
  const { data } = await api.get("/json/all");

  return { props: { data } };
}

export default Quotation;
