import Layout from "../../components/Layout";
import EconomicCalendar from "../../components/EconomicCalendar";

export default function Calendar() {
  return (
    <Layout>
      <EconomicCalendar autosize />
    </Layout>
  );
}
