import React from "react";

import Header from "../../components/Header";
import Sidebar from "../../components/Sidebar";
import CryptoCurrency from "../../components/CryptoCurrency";

export default function CryptoDetails() {
  return (
    <>
      <Header />
      <Sidebar />

      <div style={{ paddingTop: 70, height: "100%" }}>
        <CryptoCurrency autosize />
      </div>
    </>
  );
}
