import React from "react";
import { useRouter } from "next/router";
import Link from "next/link";

import axios from "axios";
import Img from "react-image";

import { MdArrowForward as ArrowIcon } from "react-icons/md";

import Header from "../../components/Header";
import Sidebar from "../../components/Sidebar";

export default function CryptoCurrencies({ cryptoCurrencies }) {
  const router = useRouter();
  //LISTAR TODAS AS CRYPTOS? /criptomoedas
  // /criptomoedas-infos [MOSTAR O WIDGET]

  return (
    <>
      <Header />

      <Sidebar />

      {cryptoCurrencies.length > 0 && (
        <div
          style={{
            marginBottom: 70,
            paddingTop: 60,
          }}
        >
          <h2 style={{ textAlign: "center", marginBottom: 10 }}>
            Principais criptomoedas |
            <Link href="/criptomoedas/details">
              <a style={{ color: "#FFF", marginLeft: 10 }}>Detalhes</a>
            </Link>
          </h2>

          <div style={{ display: "flex", flexWrap: "wrap" }}>
            {cryptoCurrencies
              .map((item) => ({
                symbol: item["s"],
                data: item["d"],
              }))
              .map((item) => (
                <div
                  className="card"
                  key={item.data[0]}
                  style={{
                    width: 300,
                    border: "1px solid #ccc",
                    borderRadius: 5,
                    margin: 10,
                    cursor: "pointer",
                  }}
                >
                  <div
                    className="card-header"
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      margin: "auto 10px",
                      alignItems: "center",
                      paddingTop: 10,
                      paddingBottom: 10,
                    }}
                  >
                    <h3>{item.data[1]}</h3>

                    <Img
                      style={{ width: 34, height: 34 }}
                      src={[
                        `https://br.tradingview.com/static/images/svg/cryptocurrencies/${item.data[0].toLowerCase()}.svg`,
                        "/images/none.svg",
                      ]}
                      alt={`${item.data[1]} logo`}
                    />
                  </div>
                  <div
                    className="card-footer"
                    style={{
                      display: "flex",
                      justifyContent: "flex-end",
                      margin: 10,
                    }}
                    onClick={() =>
                      router.push(
                        "/criptomoedas/[cryptocurrency]/[symbol]",
                        `/criptomoedas/${item.data[1].replace(/\s/g, "-")}/${
                          item.symbol
                        }`.toLowerCase()
                      )
                    }
                  >
                    <ArrowIcon color="#3fa69a" size={24} />
                  </div>
                </div>
              ))}
          </div>
        </div>
      )}
    </>
  );
}

export async function getServerSideProps() {
  const { data } = await axios({
    method: "POST",
    url: "https://scanner.tradingview.com/crypto/scan",
    data: JSON.stringify({
      filter: [
        { left: "market_cap_calc", operation: "nempty" },
        { left: "sector", operation: "nempty" },
        { left: "name", operation: "match", right: "USD$" },
      ],
      options: { lang: "pt" },
      symbols: { query: { types: [] }, tickers: [] },
      columns: ["crypto_code", "sector", "market_cap_calc"],
      sort: { sortBy: "market_cap_calc", sortOrder: "desc" },
      range: [0],
    }),
  });

  return {
    props: { cryptoCurrencies: data.data },
  };
}
