import React from "react";
import { useRouter } from "next/router";

import axios from "axios";

import MiniChart from "../../../components/MiniChart";
import Header from "../../../components/Header";
import Sidebar from "../../../components/Sidebar";

export default function Crypto() {
  const router = useRouter();

  const { symbol } = router.query;

  const Symbol = symbol?.toUpperCase();

  //BUSCAR UMA CRYPTO ESPECIFICA, DE ACORDO COM O SYMBOL?
  return (
    <>
      <Header />
      <Sidebar />

      <div style={{ paddingTop: 60 }}>
        <MiniChart symbol={Symbol} />

        <h2>Ativos que incluem a Crypto acima</h2>
      </div>
    </>
  );
}

export async function getServerSideProps({ query }) {
  console.log(query);
  // const { data } = await axios({
  //   method: "POST",
  //   url: "https://scanner.tradingview.com/crypto/scan",
  //   data: JSON.stringify({
  //     filter: [
  //       { left: "volume", operation: "nempty" },
  //       { left: "description", operation: "match", right: "^Bitcoin \\/" },
  //       { left: "name", operation: "nequal", right: "BTCUSD3M" },
  //       { left: "name", operation: "nequal", right: "BTCUSD1W" },
  //       { left: "name", operation: "nequal", right: "BTCUSD2W" },
  //       { left: "name", operation: "nequal", right: "BTCJPY1W" },
  //       { left: "name", operation: "nequal", right: "BTCJPY2W" },
  //       { left: "name", operation: "nequal", right: "BTCUSDIDX" },
  //       { left: "exchange", operation: "nequal", right: "BITMEX" },
  //       {
  //         left: "description",
  //         operation: "nmatch",
  //         right: "Calculated By Tradingview",
  //       },
  //       { left: "description", operation: "nmatch", right: "DOLLAR FORWARD" },
  //     ],
  //     options: { lang: "pt" },
  //     symbols: { query: { types: [] }, tickers: [] },
  //     columns: [
  //       "name",
  //       "close",
  //       "change",
  //       "change_abs",
  //       "high",
  //       "low",
  //       "volume",
  //       "Recommend.All",
  //       "exchange",
  //       "description",
  //       "name",
  //       "subtype",
  //       "update_mode",
  //       "pricescale",
  //       "minmov",
  //       "fractional",
  //       "minmove2",
  //     ],
  //     sort: { sortBy: "volume", sortOrder: "desc" },
  //     range: [0, 150],
  //   }),
  // });

  return {
    props: { data: [] },
  };
}
