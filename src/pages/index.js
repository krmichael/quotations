import React, { useEffect, useRef } from "react";
import { useRouter } from "next/router";
import dynamic from "next/dynamic";
import Img from "react-image";

import { MdArrowForward as ArrowIcon } from "react-icons/md";
import { FiPlus as More } from "react-icons/fi";

import axios from "axios";

import currencyFormat from "../utils/currencyFormat";
import scrollToTop from "../utils/scrollToTop";
import api from "../services/api";
import Header from "../components/Header";
import Sidebar from "../components/Sidebar";
import Arrow from "../components/Arrow";
import Loading from "../components/Loading";

const Card = dynamic(() => import("../components/Card"), {
  loading: () => <Loading>Loading card...</Loading>,
  ssr: false,
});

export default function Home({ data, cryptoCurrencies }) {
  const sliderRef = useRef();

  const router = useRouter();

  let coins = [];

  for (let coin in data) {
    coins.push(data[coin]);
  }

  function scrollHorizontal() {
    if (!cryptoCurrencies.length > 0) return;

    const slider = sliderRef.current;
    let isDown = false;
    let startX;
    let scrollLeft;

    //PRESSIONA
    slider.addEventListener("mousedown", (e) => {
      isDown = true;
      //slider.classList.add("active"); //offsetLeft = retorna a qtd de pixels a esquerda do elemento
      startX = e.pageX - slider.offsetLeft; //pageX = Retorna a coordenada horizontal do ponteiro do mouse
      scrollLeft = slider.scrollLeft; //Retorna o numero de pixels que o conteudo interno do elemento rolou na horizontal
    });

    //SOLTOU O MOUSE (parou de pressionar)
    slider.addEventListener("mouseup", () => {
      isDown = false;
      //slider.classList.remove("active");
    });

    //MOUSE SAIU DE CIMA DO ELEMENTO (scope items)
    slider.addEventListener("mouseleave", () => {
      isDown = false;
      //slider.classList.remove("active");
    });

    //MOVENDO O MOUSE
    slider.addEventListener("mousemove", (e) => {
      if (!isDown) return;

      e.preventDefault();

      const x = e.pageX - slider.offsetLeft;
      //const walk = (x - startX) * 3; //scroll-fast
      const walk = x - startX;
      slider.scrollLeft = scrollLeft - walk;
    });
  }

  useEffect(() => {
    scrollToTop();

    scrollHorizontal();
  }, []);

  return (
    <>
      <Header />

      <Sidebar />

      <div
        style={{
          height: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <div style={{ display: "flex", alignItems: "center" }}>
          <Card
            title={`do ${coins[0].name.split(" ").shift()} Agora`}
            cash={currencyFormat({ value: coins[0].bid })}
            codeActive={`${coins[0].code}-${coins[0].codein}`.toLowerCase()}
          />
        </div>
      </div>

      <div style={{ padding: 20 }}>
        <h3>Selecionados para você!</h3>

        <div style={{ display: "flex", flexWrap: "wrap" }}>
          {coins?.map((item, index) => (
            <div
              className="card"
              key={item.code + index}
              style={{
                width: 300,
                border: "1px solid #ccc",
                borderRadius: 5,
                margin: 10,
                cursor: "pointer",
              }}
            >
              <div
                className="card-header"
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  margin: "auto 10px",
                  alignItems: "center",
                  paddingTop: 10,
                  paddingBottom: 10,
                }}
              >
                <h3>{item.name}</h3>

                <Img
                  style={{ width: 34, height: 34, borderRadius: 34 }}
                  src={[
                    `/images/${item.code.toLowerCase()}.svg`,
                    "/images/none.svg",
                  ]}
                  alt={`${item.name} logo`}
                />
              </div>
              <div
                className="card-footer"
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  margin: 10,
                }}
                onClick={() =>
                  router.push(
                    `/ativos/[active]`,
                    `/ativos/cotacao-${item.name.replace(/\s/g, "-")}?symbol=${
                      item.code
                    }-${item.codein}`.toLowerCase()
                  )
                }
              >
                <ArrowIcon color="#3fa69a" size={24} />
              </div>
            </div>
          ))}
        </div>

        {cryptoCurrencies.length > 0 && (
          <>
            <h3>Criptomoedas</h3>

            <div
              style={{
                display: "flex",
                flexWrap: "nowrap",
                overflowX: "scroll",
                overflowY: "hidden",
              }}
              ref={sliderRef}
            >
              {cryptoCurrencies
                .map((item) => ({
                  symbol: item["s"],
                  data: item["d"],
                }))
                .map((item) => (
                  <div
                    onClick={() =>
                      console.log(`clicked in ${item.data[1]} card`)
                    }
                    key={item.data[0]}
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "space-around",
                      alignItems: "center",
                      borderRadius: 20,
                      minWidth: 150,
                      minHeight: 150,
                      background: "#121212",
                      margin: 10,
                      cursor: "pointer",
                    }}
                  >
                    <Img
                      style={{ width: 55, height: 55 }}
                      src={[
                        `https://br.tradingview.com/static/images/svg/cryptocurrencies/${item.data[0].toLowerCase()}.svg`,
                        "/images/none.svg",
                      ]}
                      alt={`${item.data[1]} logo`}
                    />

                    <h3 style={{ fontSize: 16 }}>{item.data[1]}</h3>
                  </div>
                ))}

              <div
                onClick={() => console.log(`Fetch all`)}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "space-around",
                  alignItems: "center",
                  borderRadius: 20,
                  minWidth: 150,
                  minHeight: 150,
                  background: "#121212",
                  margin: 10,
                  cursor: "pointer",
                }}
              >
                <More color="#3fa69a" size={50} />

                {/* <h3 style={{ fontSize: 16 }}>Todos</h3> */}
              </div>
            </div>
          </>
        )}
      </div>

      <Arrow />
    </>
  );
}

export async function getServerSideProps() {
  const { data } = await api.get("/json/all");
  // const response = await axios({
  //   method: "POST",
  //   url: "https://scanner.tradingview.com/crypto/scan",
  //   data: JSON.stringify({
  //     filter: [
  //       { left: "market_cap_calc", operation: "nempty" },
  //       { left: "sector", operation: "nempty" },
  //       { left: "name", operation: "match", right: "USD$" },
  //     ],
  //     options: { lang: "pt" },
  //     symbols: { query: { types: [] }, tickers: [] },
  //     columns: ["crypto_code", "sector", "market_cap_calc"],
  //     sort: { sortBy: "market_cap_calc", sortOrder: "desc" },
  //     range: [0, 20],
  //   }),
  // });

  return { props: { data, cryptoCurrencies: [] /* response.data.data */ } };
}
