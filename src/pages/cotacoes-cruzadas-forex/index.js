import React from "react";

import Header from "../../components/Header";
import Sidebar from "../../components/Sidebar";
import ForexCrossRates from "../../components/ForexCrossRates";

export default function Forex() {
  return (
    <>
      <Header />

      <Sidebar />

      <div style={{ paddingTop: 60 }}>
        <ForexCrossRates />
      </div>
    </>
  );
}
