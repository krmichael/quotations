import React, { useContext } from "react";

import { useRouter } from "next/router";
import dynamic from "next/dynamic";

import ThemeContext from "../../theme/context";
import Loading from "../../components/Loading";

const Chart = dynamic(() => import("../../components/TradingViewChart"), {
  loading: () => <Loading>Loading chart...</Loading>,
  ssr: false,
});

export default function Redirect() {
  const router = useRouter();
  const { theme } = useContext(ThemeContext);

  const { tvwidgetsymbol } = router.query;

  return (
    <Chart
      autosize={true}
      symbol={tvwidgetsymbol || "USDBRL"}
      theme={theme.light ? "Light" : "Dark"}
      locale="br"
      interval={60}
      style="3"
      toolbar_bg={theme.light ? "#333" : "#f1f3f6"}
      allow_symbol_change={false}
    />
  );
}
