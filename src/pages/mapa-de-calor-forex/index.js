import React from "react";

import Header from "../../components/Header";
import Sidebar from "../../components/Sidebar";
import ForexHeatMap from "../../components/ForexHeatMap";

export default function ForexMap() {
  return (
    <>
      <Header />
      <Sidebar />

      <div style={{ paddingTop: 60 }}>
        <ForexHeatMap />
      </div>
    </>
  );
}
