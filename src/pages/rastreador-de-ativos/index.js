import React from "react";

import Header from "../../components/Header";
import Sidebar from "../../components/Sidebar";
import TrackerActives from "../../components/TrackerActives";

export default function Tracker() {
  return (
    <>
      <Header />
      <Sidebar />

      <div style={{ paddingTop: 60 }}>
        <TrackerActives />
      </div>
    </>
  );
}
