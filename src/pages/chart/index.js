import React, { useContext, useEffect, useState } from "react";
import styled, { css } from "styled-components";

import Helmet from "react-helmet";
import dynamic from "next/dynamic";
import router from "next/router";

import api from "../../services/api";
import isMobile from "../../utils/isMobile";
import isServer from "../../utils/isServer";

import Header from "../../components/Header";
import Sidebar from "../../components/Sidebar";
import Loading from "../../components/Loading";

import ThemeContext from "../../theme/context";

const Chart = dynamic(() => import("../../components/TradingViewChart"), {
  loading: () => <Loading>Loading chart...</Loading>,
  ssr: false,
});

function Graph({ error, symbol, name }) {
  if (!isServer && error) router.replace("/404", "/page-not-found");

  const [fullscreen, setFullscreen] = useState(false);

  const { theme } = useContext(ThemeContext);

  useEffect(() => {
    if (isMobile) {
      setFullscreen(true);
    }
  }, []);

  return (
    <>
      {!fullscreen && <Header />}

      <Sidebar />

      <Container fullscreen={fullscreen}>
        <Helmet
          title={`Gráfico cotação ${name} | Cotação.com`}
          meta={[
            {
              property: "og:title",
              content: `Cotação ${name}`,
            },
          ]}
        />

        <ChartScope fullscreen={fullscreen}>
          <Chart
            autosize={true}
            symbol={symbol || "USDBRL"}
            theme={theme.light ? "Light" : "Dark"}
            locale="br"
            interval={60}
            style="3"
            toolbar_bg={theme.light ? "#333" : "#f1f3f6"}
            allow_symbol_change={false}
          />
        </ChartScope>
      </Container>
    </>
  );
}

export async function getServerSideProps({ query }) {
  if (!query || query === {} || !query?.symbol) {
    return {
      props: { error: true },
    };
  }

  try {
    const symbol = query.symbol.toUpperCase();

    const { data } = await api.get(`/json/all/${symbol}`);

    const coins = [];

    for (let coin in data) {
      coins.push(data[coin]);
    }

    const name = coins?.map((item) => item.name).join("");

    return {
      props: { error: false, symbol, name },
    };
  } catch (error) {
    return {
      props: { error: true },
    };
  }
}

export const Container = styled.div.attrs((props) => ({
  fullscreen: props.fullscreen,
}))`
  height: 100%;
  /* margin-top: ${(props) => (!props.fullscreen ? 20 : 0)}px; */

  ${(props) =>
    !props.fullscreen &&
    css`
      height: calc(100% - 100px);
      display: flex;
      justify-content: center;
      align-items: center;
    `}
`;

export const ChartScope = styled.div.attrs((props) => ({
  fullscreen: props.fullscreen,
}))`
  width: 100%;
  max-width: ${(props) => !props.fullscreen && 980}px;
  height: 100%;
  max-height: ${(props) => !props.fullscreen && 582}px;
`;

export default Graph;
