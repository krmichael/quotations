import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    border: 0;
    outline: none;
    box-sizing: border-box;
  }

  ::-webkit-scrollbar {
    width: 0px !important;
    height: 0px !important;
  }

  html,
  body,
  #__next {
    width: 100%;
    height: 100%;
    background-color: ${(props) => props.theme.background};
    color: ${(props) => props.theme.foreground};
    font-size: 18px;
    font-family: Arial, Helvetica, sans-serif;
  }
`;

export default GlobalStyle;
