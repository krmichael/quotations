import { createStore } from "redux";

import rootReducer from "./reducers";

const INITIAL_STATE = {
  sidebar: {
    isOpen: !!false,
  },
};

export const initializeStore = (preloadedState = INITIAL_STATE) =>
  createStore(rootReducer, preloadedState);
