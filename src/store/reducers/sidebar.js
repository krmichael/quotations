const INITIAL_STATE = {
  isOpen: !!false,
};

export default function Sidebar(state = INITIAL_STATE, action) {
  switch (action.type) {
    case "TOGGLE_SIDEBAR":
      return {
        ...state,
        isOpen: action.payload.isOpen,
      };

    default:
      return state;
  }
}
