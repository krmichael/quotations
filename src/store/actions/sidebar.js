export function toggleSidebar(isOpen) {
  return {
    type: "TOGGLE_SIDEBAR",
    payload: { isOpen },
  };
}
