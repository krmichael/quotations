import isServer from "./isServer";

const isMobile = !isServer && /mobile/i.test(navigator.userAgent);

export default isMobile;
