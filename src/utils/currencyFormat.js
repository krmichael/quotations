export default function currencyFormat({
  locale = "pt-BR",
  style = "currency",
  currency = "BRL",
  value
}) {
  return Intl.NumberFormat(locale, { style, currency }).format(value);
}
