import React, { useState, useEffect } from "react";

import { ScopeArrow, Arrow } from "./styles";

import isServer from "../../utils/isServer";

export default function ArrowDown() {
  const [arrowVisible, setArrowVisible] = useState(true);

  function handleArrow() {
    !isServer && window.pageYOffset > 5
      ? setArrowVisible(false)
      : setArrowVisible(true);
  }

  useEffect(() => {
    window.addEventListener("scroll", handleArrow);

    return () => {
      window.removeEventListener("scroll", handleArrow);
    };
  }, []);

  return (
    <ScopeArrow>
      <Arrow visible={arrowVisible} />
    </ScopeArrow>
  );
}
