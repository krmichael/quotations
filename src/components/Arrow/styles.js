import styled, { keyframes } from "styled-components";

const arrow = keyframes`
  0% {
    opacity: 0;
    filter: alpha(opacity=0)
  }
  100% {
    opacity: 1;
    filter: alpha(opacity=100);
    transform: translate(-10px -10px);
  }
`;

export const ScopeArrow = styled.div`
  display: flex;
  justify-content: center;
`;

export const Arrow = styled.div.attrs((props) => ({
  visible: props.visible,
}))`
  position: absolute;
  display: ${(props) => (props.visible ? "block" : "none")};
  bottom: 10px;
  width: 30px;
  height: 30px;
  transform: rotate(45deg);
  border-left: none;
  border-top: none;
  border-right: 3px solid #3fa69a;
  border-bottom: 3px solid #3fa69a;

  &::before {
    position: absolute;
    content: "";
    width: 20px;
    height: 20px;
    border-left: none;
    border-top: none;
    border-right: 1px solid #3fa69a;
    border-bottom: 1px solid #3fa69a;
    animation: 1.5s ${arrow} infinite;
  }
`;
