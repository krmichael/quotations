import React, { useEffect, useRef } from "react";
import PropTypes from "prop-types";

const Themes = {
  LIGHT: "light",
  DARK: "dark",
};
export default function Analyze(props) {
  const containerRef = useRef();

  function init() {
    const { ...widgetConfig } = props;
    const config = { ...widgetConfig };

    if (config.autosize) {
      delete config.width;
      delete config.height;

      config.width = "100%";
      config.height = "100%";
    }

    const script = document.createElement("script");
    script.type = "text/javascript";
    script.async = true;
    script.src =
      "https://s3.tradingview.com/external-embedding/embed-widget-technical-analysis.js";
    script.innerText = JSON.stringify(config);

    containerRef.current.appendChild(script);
  }

  function clean() {
    containerRef.current.innerHTML = "";
  }

  useEffect(() => {
    init();

    return () => {
      clean();
    };
  }, []);

  return (
    <div className="tradingview-widget-container" ref={containerRef}>
      <div className="tradingview-widget-container__widget"></div>
    </div>
  );
}

Analyze.propTypes = {
  symbol: PropTypes.string.isRequired,
  interval: PropTypes.oneOf(["1m", "5m", "15m", "1h", "4h", "1D", "1W", "1M"]),
  isTransparent: PropTypes.bool,
  showIntervalTabs: PropTypes.bool,
  locale: PropTypes.string,
  colorTheme: PropTypes.oneOf([Themes.DARK, Themes.LIGHT]),
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  autosize: PropTypes.bool,
  largeChartUrl: PropTypes.string,
};

Analyze.defaultProps = {
  interval: "1m",
  isTransparent: false,
  showIntervalTabs: true,
  locale: "br",
  colorTheme: Themes.DARK,
  width: 425,
  height: 450,
  autosize: false,
  largeChartUrl: "",
};
