import React, { useEffect, useRef } from "react";

//import PropTypes from "prop-types";

const Themes = {
  DARK: "dark",
  LIGHT: "light",
};

export default function Stocks(props) {
  const containerRef = useRef();

  function init() {
    const { ...widgetConfig } = props;
    const config = { ...widgetConfig };

    if (config.autosize) {
      delete config.width;
      delete config.height;

      config.width = "100%";
      config.height = "100%";
    }

    const script = document.createElement("script");
    script.type = "text/javascript";
    script.async = true;
    script.src =
      "https://s3.tradingview.com/external-embedding/embed-widget-hotlists.js";
    script.innerHTML = JSON.stringify(config);

    containerRef.current.appendChild(script);
  }

  function clean() {
    containerRef.current.innerHTML = "";
  }

  useEffect(() => {
    init();

    return () => {
      clean();
    };
  }, []);

  return (
    <div className="tradingview-widget-container" ref={containerRef}>
      <div className="tradingview-widget-container__widget"></div>
    </div>
  );
}

// Stocks.propTypes = {
//   exchange: 'BMFBOVESPA',
//   dateRange: "12m",
//   showChart: true,
//   locale: "br",
//   colorTheme: Themes.DARK,
//   plotLineColorGrowing: 'rgba(33, 150, 243, 1)',
//   plotLineColorFalling: 'rgba(33, 150, 243, 1)',
//   gridLineColor: 'rgba(240, 243, 250, 1)',
//   scaleFontColor: 'rgba(120, 123, 134, 1)',
//   belowLineFillColorGrowing: 'rgba(33, 150, 243, 0.12)',
//   belowLineFillColorFalling: 'rgba(33, 150, 243, 0.12)',
//   symbolActiveColor: 'rgba(33, 150, 243, 0.12)',
//   width: 350,
//   height: 220,
//   autosize: false,
//   isTransparent: false,
//   largeChartUrl: "",
// };

Stocks.defaultProps = {
  exchange: "BMFBOVESPA",
  dateRange: "12m",
  showChart: true,
  locale: "br",
  colorTheme: Themes.DARK,
  plotLineColorGrowing: "rgba(33, 150, 243, 1)",
  plotLineColorFalling: "rgba(33, 150, 243, 1)",
  gridLineColor: "rgba(240, 243, 250, 1)",
  scaleFontColor: "rgba(120, 123, 134, 1)",
  belowLineFillColorGrowing: "rgba(33, 150, 243, 0.12)",
  belowLineFillColorFalling: "rgba(33, 150, 243, 0.12)",
  symbolActiveColor: "rgba(33, 150, 243, 0.12)",
  width: 350,
  height: 220,
  autosize: false,
  isTransparent: false,
  largeChartUrl: "",
};
