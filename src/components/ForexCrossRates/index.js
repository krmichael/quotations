import React, { useEffect, useRef } from "react";

import PropTypes from "prop-types";

const Themes = {
  DARK: "dark",
  LIGHT: "light",
};

export default function ForexCrossRates(props) {
  const containerRef = useRef();

  function init() {
    const { ...widgetConfig } = props;
    const config = { ...widgetConfig };

    if (config.autosize) {
      delete config.width;
      delete config.height;

      config.width = "100%";
      config.height = "100%";
    }

    const script = document.createElement("script");
    script.type = "text/javascript";
    script.async = true;
    script.src =
      "https://s3.tradingview.com/external-embedding/embed-widget-forex-cross-rates.js";
    script.innerHTML = JSON.stringify(config);

    containerRef.current.appendChild(script);
  }

  function clean() {
    containerRef.current.innerHTML = "";
  }

  useEffect(() => {
    init();

    return () => {
      clean();
    };
  }, []);

  return (
    <div className="tradingview-widget-container" ref={containerRef}>
      <div className="tradingview-widget-container__widget"></div>
    </div>
  );
}

ForexCrossRates.propTypes = {
  currencies: PropTypes.arrayOf(PropTypes.string),
  locale: PropTypes.string,
  colorTheme: PropTypes.oneOf([Themes.DARK, Themes.LIGHT]),
  isTransparent: PropTypes.bool,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  autosize: PropTypes.bool,
  //largeChartUrl: PropTypes.string,
};

ForexCrossRates.defaultProps = {
  //PERMITIR QUE O USUARIO ADICIONE OU REMOVA MOEDAS (Ver todas no site)
  currencies: ["EUR", "USD", "JPY", "GBP", "CHF", "AUD", "CAD", "NZD", "CNY"],
  locale: "br",
  colorTheme: Themes.DARK,
  isTransparent: false,
  width: 770,
  height: 400,
  autosize: false,
  //largeChartUrl: "", BUG, REDIRECIONANDO PARA UMA PAGINA QUE LISTA TODOS OS ATIVOS FOREX
};
