import styled from "styled-components";

export const Description = styled.div`
  width: 100%;
  max-width: 1120px;
  margin: 0px auto;
  padding: 15px;
`;

export const DescriptionTitle = styled.h3`
  text-align: center;
  padding: 15px;
  font-size: 40px;
  letter-spacing: -4px;
  color: ${(props) => props.theme.foreground};
`;

export const DescriptionText = styled.p`
  text-align: justify;
  letter-spacing: 1px;
  padding: 10px 5px;
`;
