import React from "react";

import { Description, DescriptionTitle, DescriptionText } from "./styles";

export default function Content({ content }) {
  return (
    <>
      {content && content.description && (
        <Description>
          <DescriptionTitle>O que é {content.name}</DescriptionTitle>

          <DescriptionText>{content.description}</DescriptionText>
        </Description>
      )}
    </>
  );
}
