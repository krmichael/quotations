import styled from "styled-components";

import { AiOutlineClose } from "react-icons/ai";

export const Container = styled.div.attrs((props) => ({
  visible: props.visible,
}))`
  position: fixed;
  display: ${(props) => (props.visible ? "block" : "none")};
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.6);
`;

export const SidebarMenu = styled.div`
  position: relative;
  padding: 7px;
  top: 0px;
  width: 270px;
  height: 100%;
  background: ${(props) => props.theme.background};
`;

export const MenuHeader = styled.div`
  text-align: right;
`;

export const ButtonClose = styled(AiOutlineClose)`
  cursor: pointer;
`;

export const List = styled.ul`
  list-style-type: none;
  margin-top: 20px;
`;

export const ListItem = styled.li`
  margin: 10px 0px;
  padding: 7px 3px;
  border-radius: 3px;
  cursor: pointer;
`;

export const ListItemLink = styled.a`
  text-decoration: none;
`;
