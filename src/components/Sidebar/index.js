import React, { useContext } from "react";
import { useSelector, useDispatch } from "react-redux";
import Link from "next/link";

import { withRedux } from "../../config/redux";

import * as sidebarActions from "../../store/actions/sidebar";

import {
  Container,
  SidebarMenu,
  MenuHeader,
  ButtonClose,
  List,
  ListItem,
  ListItemLink,
} from "./styles";

import ThemeContext from "../../theme/context";

const LINK_ITEMS = [
  { id: 1, label: "Ínicio", route: "/" },
  { id: 2, label: "Moedas", route: "/moedas" },
  { id: 3, label: "Crypto", route: "/criptomoedas" },
  { id: 4, label: "Índices", route: "/indices" },
  { id: 5, label: "Futuros", route: "/futuros" },
  { id: 6, label: "Títulos", route: "/titulos" },
  { id: 7, label: "Ações", route: "/acoes" },
  { id: 8, label: "Commodities", route: "/commodities" },
  { id: 9, label: "Forex", route: "/forex" },
  { id: 10, label: "News", route: "/noticias" },
  { id: 11, label: "Rastreador de ativos", route: "/rastreador-de-ativos" },
  { id: 12, label: "Dados de mercado", route: "/dados-de-mercado" },
];

function Sidebar() {
  const dispatch = useDispatch();
  const { theme } = useContext(ThemeContext);

  const isOpen = useSelector((state) => state.sidebar.isOpen);

  function closeMenu() {
    dispatch(sidebarActions.toggleSidebar(false));
  }

  return (
    <Container visible={isOpen}>
      <SidebarMenu>
        <MenuHeader>
          <ButtonClose
            onClick={closeMenu}
            color={theme.foreground}
            size={24}
            title={"Fechar menu"}
          />
        </MenuHeader>

        <List>
          {LINK_ITEMS.map((item) => (
            <ListItem key={item.id} onClick={closeMenu}>
              <Link href={item.route}>
                <ListItemLink>{item.label}</ListItemLink>
              </Link>
            </ListItem>
          ))}
        </List>
      </SidebarMenu>
    </Container>
  );
}

export default withRedux(Sidebar);
