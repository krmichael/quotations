import React, { useEffect, useRef } from "react";

import PropTypes from "prop-types";

const Themes = {
  DARK: "dark",
  LIGHT: "light",
};

export default function MiniChart(props) {
  const containerRef = useRef();

  function init() {
    const { ...widgetConfig } = props;
    const config = { ...widgetConfig };

    if (config.autosize) {
      delete config.width;
      delete config.height;

      config.width = "100%";
      config.height = "100%";
    }

    const script = document.createElement("script");
    script.type = "text/javascript";
    script.async = true;
    script.src =
      "https://s3.tradingview.com/external-embedding/embed-widget-mini-symbol-overview.js";
    script.innerHTML = JSON.stringify(config);

    containerRef.current.appendChild(script);
  }

  function clean() {
    containerRef.current.innerHTML = "";
  }

  useEffect(() => {
    init();

    return () => {
      clean();
    };
  }, []);

  return (
    <div className="tradingview-widget-container" ref={containerRef}>
      <div className="tradingview-widget-container__widget"></div>
    </div>
  );
}

MiniChart.propTypes = {
  symbol: PropTypes.string.isRequired,
  locale: PropTypes.string,
  dateRange: PropTypes.string, //oneOfType ver os tipos no site..
  colorTheme: PropTypes.oneOf([Themes.DARK, Themes.LIGHT]),
  trendLineColor: PropTypes.string,
  underLineColor: PropTypes.string,
  isTransparent: PropTypes.bool,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  autosize: PropTypes.bool,
  largeChartUrl: PropTypes.string,
};

MiniChart.defaultProps = {
  symbol: "GEMINI:BTCUSD",
  locale: "br",
  dateRange: "12m",
  colorTheme: Themes.DARK,
  trendLineColor: "#37a6ef",
  underLineColor: "rgba(55, 166, 239, 0.15)",
  isTransparent: false,
  width: 350,
  height: 220,
  autosize: false,
  largeChartUrl: "",
};
