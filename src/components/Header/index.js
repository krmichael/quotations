import React, { useContext } from "react";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";

import { withRedux } from "../../config/redux";

import * as sidebarActions from "../../store/actions/sidebar";

import {
  Container,
  ScopeLogo,
  Logo,
  ScopeMenu,
  Menu,
  MenuText,
} from "./styles";

import ThemeContext from "../../theme/context";

function Header() {
  const dispatch = useDispatch();

  const { theme } = useContext(ThemeContext);
  const router = useRouter();

  function openMenu() {
    dispatch(sidebarActions.toggleSidebar(true));
  }

  return (
    <>
      <Container>
        <ScopeMenu onClick={openMenu}>
          <Menu color={theme.foreground} size={30} title={"Menu"} />
          <MenuText>Menu</MenuText>
        </ScopeMenu>

        <ScopeLogo onClick={() => router.push("/")}>
          <Logo title={"cotações.app"} />
        </ScopeLogo>
      </Container>
    </>
  );
}

export default withRedux(Header);
