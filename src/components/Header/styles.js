import styled from "styled-components";

import { AiOutlineMenuFold, AiOutlineAliwangwang } from "react-icons/ai";

export const Container = styled.div`
  position: fixed;
  width: 100%;
  padding: 5px 20px 12px;
  display: flex;
  align-items: center;
  background: #2f2f2f;
`;

export const Scope = styled.div`
  width: 50%;
`;

export const ScopeMenu = styled(Scope)`
  display: flex;
  align-items: center;
  cursor: pointer;
`;

export const Menu = styled(AiOutlineMenuFold)``;

export const MenuText = styled.h3`
  font-size: 16px;
  margin-left: 5px;
  letter-spacing: 0.98px;
`;

export const ScopeLogo = styled(Scope)`
  cursor: pointer;
`;

export const Logo = styled(AiOutlineAliwangwang).attrs({
  color: "#3fa69a",
  size: 50,
})``;
