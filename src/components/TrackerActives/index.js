import React, { useEffect, useRef } from "react";

import PropTypes from "prop-types";

const Themes = {
  DARK: "dark",
  LIGHT: "light",
};

export default function TrackerActives(props) {
  const containerRef = useRef();

  function init() {
    const { ...widgetConfig } = props;
    const config = { ...widgetConfig };

    if (config.autosize) {
      delete config.width;
      delete config.height;

      config.width = "100%";
      config.height = "100%";
    }

    const script = document.createElement("script");
    script.type = "text/javascript";
    script.async = true;
    script.src =
      "https://s3.tradingview.com/external-embedding/embed-widget-screener.js";
    script.innerHTML = JSON.stringify(config);

    containerRef.current.appendChild(script);
  }

  function clean() {
    containerRef.current.innerHTML = "";
  }

  useEffect(() => {
    init();

    return () => {
      clean();
    };
  }, []);

  return (
    <div className="tradingview-widget-container" ref={containerRef}>
      <div className="tradingview-widget-container__widget"></div>
    </div>
  );
}

TrackerActives.propTypes = {
  defaultColumn: PropTypes.string,
  defaultScreen: PropTypes.string,
  market: PropTypes.string,
  showToolbar: PropTypes.bool,
  colorTheme: PropTypes.oneOf([Themes.DARK, Themes.LIGHT]),
  locale: PropTypes.string,
  isTransparent: PropTypes.bool,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  autosize: PropTypes.bool,
  largeChartUrl: PropTypes.string,
};

TrackerActives.defaultProps = {
  defaultColumn: "overview",
  defaultScreen: "general",
  market: "us",
  showToolbar: true,
  colorTheme: Themes.DARK,
  locale: "br",
  isTransparent: false,
  width: 1100,
  height: 512,
  autosize: false,
  largeChartUrl: "",
};
