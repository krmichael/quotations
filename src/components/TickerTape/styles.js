import styled from "styled-components";

export const Container = styled.div`
  position: fixed;
  width: 100%;
  bottom: 0;
`;
