import React, { useEffect, useRef } from "react";

import PropTypes from "prop-types";

import { Container } from "./styles";

const Themes = {
  DARK: "dark",
  LIGHT: "light",
};

const Mode = {
  ADAPTIVE: "adaptive",
  REGULAR: "regular",
  COMPACT: "compact",
};

export default function TickerTape(props) {
  const containerRef = useRef();

  function init() {
    const script = document.createElement("script");
    script.type = "text/javascript";
    script.async = true;
    script.src =
      "https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js";
    script.innerHTML = JSON.stringify(props);

    containerRef.current.appendChild(script);
  }

  function clean() {
    containerRef.current.innerHTML = "";
  }

  useEffect(() => {
    init();

    return () => {
      clean();
    };
  }, []);

  return (
    <Container>
      <div className="tradingview-widget-container" ref={containerRef}>
        <div className="tradingview-widget-container__widget" />
      </div>
    </Container>
  );
}

TickerTape.propTypes = {
  symbols: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.string)),
  colorTheme: PropTypes.oneOf([Themes.DARK, Themes.LIGHT]),
  isTransparent: PropTypes.bool,
  displayMode: PropTypes.oneOf([Mode.ADAPTIVE, Mode.REGULAR, Mode.COMPACT]),
  locale: PropTypes.string,
  largeChartUrl: PropTypes.string,
};

TickerTape.defaultProps = {
  symbols: [
    {
      proName: "FOREXCOM:SPXUSD",
      title: "S&P 500",
    },
    {
      proName: "FOREXCOM:NSXUSD",
      title: "Nasdaq 1000",
    },
    {
      proName: "FX_IDC:EURUSD",
      title: "EUR/USD",
    },
    {
      proName: "BITSTAMP:BTCUSD",
      title: "BTC/USD",
    },
    {
      proName: "BITSTAMP:ETHUSD",
      title: "ETH/USD",
    },
  ],
  colorTheme: Themes.DARK,
  isTransparent: false,
  displayMode: Mode.ADAPTIVE,
  locale: "br",
  largeChartUrl: "",
};
