import React, { useEffect, useRef } from "react";
import PropTypes from "prop-types";

const Themes = {
  DARK: "dark",
  LIGHT: "light",
};

export default function EconomicCalendar(props) {
  const containerRef = useRef();

  function init() {
    const { ...widgetConfig } = props;
    const config = { ...widgetConfig };

    if (config.autosize) {
      delete config.height;
      delete config.width;

      config.height = "100%";
      config.width = "100%";
    }

    const script = document.createElement("script");
    script.type = "text/javascript";
    script.async = true;
    script.src =
      "https://s3.tradingview.com/external-embedding/embed-widget-events.js";
    script.innerHTML = JSON.stringify(config);

    containerRef.current.appendChild(script);
  }

  function clean() {
    containerRef.current.innerHTML = "";
  }

  useEffect(() => {
    init();

    return () => {
      clean();
    };
  }, []);

  return (
    <div className="tradingview-widget-container" ref={containerRef}>
      <div className="tradingview-widget-container__widget"></div>
    </div>
  );
}

EconomicCalendar.propTypes = {
  colorTheme: PropTypes.oneOf([Themes.DARK, Themes.LIGHT]),
  isTransparent: PropTypes.bool,
  locale: PropTypes.string,
  importanceFilter: PropTypes.string,
  currencyFilter: PropTypes.string,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  autosize: PropTypes.bool,
};

EconomicCalendar.defaultProps = {
  colorTheme: Themes.DARK,
  isTransparent: false,
  locale: "br",
  importanceFilter: "0,1",
  width: 510,
  height: 600,
  autosize: false,
};
