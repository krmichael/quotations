import React from "react";
import { useRouter } from "next/router";

import Image from "react-image";

import { Card, Content, Title, Footer, Value, Action } from "./styles";

export default function CardComponent({ title, cash, codeActive }) {
  const router = useRouter();

  const codeImage = codeActive.split("-").shift();

  return (
    <Card>
      <Image
        style={{ width: 55, height: 55, borderRadius: 55 }}
        src={[`/images/${codeImage}.svg`, "/images/none.svg"]}
        alt={title}
      />

      <Content>
        <Title>Cotação {title}</Title>

        <Footer>
          <Value>{cash}</Value>
          <Action
            title={`Gráfico com cotação ${
              title?.replace(/Cotação/, "").replace(/Agora/, "") || "Dolar"
            }`}
            onClick={() =>
              router.push({ pathname: "/chart", query: { symbol: codeActive } })
            }
          />
        </Footer>
      </Content>
    </Card>
  );
}
