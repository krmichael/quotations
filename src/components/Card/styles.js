import styled from "styled-components";

import { IoIosArrowRoundForward } from "react-icons/io";

export const Card = styled.div`
  display: flex;
  align-items: center;
  padding: 4px 10px;
  background: #2f2f2f;
  border-radius: 7px;
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  margin: 7px 20px 0px;
`;

export const Title = styled.h3``;

export const Footer = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 7px;
`;

export const Value = styled.h3`
  font-size: 14px;
  margin-top: 7px;
  letter-spacing: 0.98px;
  text-align: center;
`;

export const Action = styled(IoIosArrowRoundForward).attrs({
  color: "#3fa69a",
  size: 40,
})`
  margin-top: 7px;
  cursor: pointer;
  display: flex;
  align-self: flex-end;
`;
