import styled from "styled-components";

export const Container = styled.div`
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Loading = styled.span`
  color: ${(props) => props.theme.foreground};
  font-size: 24px;
  font-weight: bold;
`;
