import { Container, Loading } from "./styles";

export default function LoadingComponent({ children }) {
  return (
    <Container>
      <Loading>{children}</Loading>
    </Container>
  );
}
