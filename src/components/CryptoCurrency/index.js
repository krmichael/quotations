import React, { useEffect, useRef } from "react";
import PropTypes from "prop-types";

const Themes = {
  DARK: "dark",
  LIGHT: "light",
};

const ColumnsTypes = {
  ALL: "overview",
  PERFORMANCE: "performance",
  OSCILLATORS: "oscillators",
  MOVING: "moving_averages",
};

const Currency = {
  USD: "USD",
  BTC: "BTC",
};

export default function CryptoCurrency(props) {
  const containerRef = useRef();

  function init() {
    const { ...widgetConfig } = props;
    const config = { ...widgetConfig };

    if (config.autosize) {
      delete config.height;
      delete config.width;

      config.height = "100%";
      config.width = "100%";
    }

    const script = document.createElement("script");
    script.type = "text/javascript";
    script.async = true;
    script.src =
      "https://s3.tradingview.com/external-embedding/embed-widget-screener.js";
    script.innerHTML = JSON.stringify(config);

    containerRef.current.appendChild(script);
  }

  function clean() {
    containerRef.current.innerHTML = "";
  }

  useEffect(() => {
    init();

    return () => {
      clean();
    };
  }, []);

  return (
    <div className="tradingview-widget-container" ref={containerRef}>
      <div className="tradingview-widget-container__widget"></div>
    </div>
  );
}

CryptoCurrency.propTypes = {
  defaultColumn: PropTypes.oneOf([
    ColumnsTypes.ALL,
    ColumnsTypes.PERFORMANCE,
    ColumnsTypes.OSCILLATORS,
    ColumnsTypes.MOVING,
  ]),
  screener_type: PropTypes.string,
  displayCurrency: PropTypes.oneOf([Currency.USD, Currency.BTC]),
  colorTheme: PropTypes.oneOf([Themes.DARK, Themes.LIGHT]),
  locale: PropTypes.string,
  isTransparent: PropTypes.bool,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  autosize: PropTypes.bool,
};

CryptoCurrency.defaultProps = {
  defaultColumn: ColumnsTypes.ALL,
  screener_type: "crypto_mkt",
  displayCurrency: Currency.USD,
  colorTheme: Themes.DARK,
  locale: "br",
  isTransparent: false,
  width: 1000,
  height: 490,
  autosize: false,
};
