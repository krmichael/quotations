import React, { useEffect, useRef } from "react";

import PropTypes from "prop-types";

const Themes = {
  DARK: "dark",
  LIGHT: "light",
};

export default function Ticker(props) {
  const containerRef = useRef();

  function init() {
    const script = document.createElement("script");
    script.type = "text/javascript";
    script.async = true;
    script.src =
      "https://s3.tradingview.com/external-embedding/embed-widget-tickers.js";
    script.innerHTML = JSON.stringify(props);

    containerRef.current.appendChild(script);
  }

  function clean() {
    containerRef.current.innerHTML = "";
  }

  useEffect(() => {
    init();

    return () => {
      clean();
    };
  }, []);

  return (
    <div className="tradingview-widget-container" ref={containerRef}>
      <div className="tradingview-widget-container__widget"></div>
    </div>
  );
}

Ticker.propTypes = {
  symbols: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.string)),
  colorTheme: PropTypes.oneOf([Themes.DARK, Themes.LIGHT]),
  isTransparent: PropTypes.bool,
  locale: PropTypes.string,
  largeChartUrl: PropTypes.string,
};

Ticker.defaultProps = {
  symbols: [
    {
      proName: "FOREXCOM:SPXUSD",
      title: "S&P 500",
    },
    {
      proName: "FOREXCOM:NSXUSD",
      title: "Nasdaq 1000",
    },
    {
      proName: "FX_IDC:EURUSD",
      title: "EUR/USD",
    },
    {
      proName: "BITSTAMP:BTCUSD",
      title: "BTC/USD",
    },
    {
      proName: "BITSTAMP:ETHUSD",
      title: "ETH/USD",
    },
    {
      description: "IBOVESPA",
      proName: "INDEX:IBOV",
    },
    {
      description: "USD/BRL",
      proName: "FX_IDC:USDBRL",
    },
    {
      description: "BTC/BRL",
      proName: "MERCADO:BTCBRL",
    },
  ],
  colorTheme: Themes.DARK,
  isTransparent: false,
  locale: "br",
  largeChartUrl: "",
};
